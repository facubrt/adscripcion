/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "blinking_tec.h"       /* <= own header */
#include "board.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000

/*==================[internal data definition]===============================*/



/*==================[internal functions declaration]=========================*/

void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

int main(void)
{
	uint32_t tecla;
	Board_Init();
	Init_Leds();
	Init_Switches();

    while(1)
    {
    	tecla = Read_Switches();
    	switch(tecla)
    	{
    		case TEC1:
    			Led_Toggle(RGB_B_LED);
    			Led_Off(RGB_R_LED);
    			Led_Off(RGB_G_LED);
    		break;
    		case TEC2:
    			Led_Toggle(RED_LED);
    			Led_Off(RGB_R_LED);
    			Led_Off(RGB_G_LED);
    		break;
    		case TEC3:
    			Led_Toggle(YELLOW_LED);
    			Led_Off(RGB_R_LED);
    			Led_Off(RGB_G_LED);
    		break;
    		case TEC4:
    			Led_Toggle(GREEN_LED);
    			Led_Off(RGB_R_LED);
    			Led_Off(RGB_G_LED);
    		break;
    		default:
    			Led_Toggle(RGB_R_LED);
    			Led_Toggle(RGB_G_LED);

    			Led_Off(RGB_B_LED);
    			Led_Off(RED_LED);
    			Led_Off(YELLOW_LED);
    			Led_Off(GREEN_LED);
    	}
		Delay();
	}
    
	return 0;
}

/*==================[end of file]============================================*/

