/*
 * hc_sr4.c
 *
 *  Created on: 23 de ago. de 2018
 *      Author: facub
 */

#include "hc_sr4.h"
#include "chip.h"
#include "retardos.h"

#define GROUP(n) ((uint8_t) n)
#define PORT(n) ((uint8_t) n)
#define PIN(n) ((uint8_t) n)
#define FUNC(n) ((uint8_t) n)
#define TIMER(n) ((uint8_t) n)
#define IN		0
#define OUT 	1

void InicializarHcSr4(void)
{
	Chip_GPIO_Init(LPC_GPIO_PORT);

	//T_FIL2 (ECHO) FUNCION GPIO
	Chip_SCU_PinMux(GROUP(4),PIN(2),MD_EZI|MD_ZI,FUNC(0));
	//T_FIL3 (TRIGGER) FUNCION GPIO
	Chip_SCU_PinMux(GROUP(4),PIN(3),MD_PLN,FUNC(0));

	//ECHO, MODO ENTRADA
	Chip_GPIO_SetDir(LPC_GPIO_PORT,PORT(2),1<<PIN(2),IN);
	//TRIGGER, MODO SALIDA
	Chip_GPIO_SetDir(LPC_GPIO_PORT,PORT(2),1<<PIN(3),OUT);
}
uint16_t LeerDistanciaCentimetro(void)
{	//SE ENVIA LA SEÑAL ULTRASONIDO Y SE OBTIENE LA DIFERENCIA DE TIEMPO DE LA RECEPCION
	uint16_t t=0;
	uint16_t d=0;
	uint16_t cont=0;

	/* señal trigger*/
	//SETEO INICIALMENTE EN BAJO durante 5u por estabilidad
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,PORT(2),PIN(3));
	delayUs(TIMER(0),5);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,PORT(2),PIN(3));
	delayUs(TIMER(0),10);
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,PORT(2),PIN(3));

	/*obtengo el tiempo de echo*/
	//tengo que esperar a que echo reciba una señal en alto,
	//y empezar a contar su duracion hasta que vuelva a low

	//MIENTRAS ECHO ESTÉ EN BAJO NO HACER NADA
	do
	{

	}
	while (!Chip_GPIO_ReadPortBit(LPC_GPIO_PORT,PORT(2),PIN(2)));

	while((Chip_GPIO_ReadPortBit(LPC_GPIO_PORT,PORT(2),PIN(2)))&&(t<7540)) //la limitación de t es para limitar la distancia de medición a 130cm ya que 130cm*58=7540us
	{
		t++;
		delayUs(TIMER(0),1);//retardo de 1u para obtener el tiempo en u
	}

	//solamente consideramos la mitad del tiempo en alto, que es la distancia hasta alcanzar el objeto sin
	//contar el tiempo de regreso

	/*distancia en centimetros*/
	d=t/58;

	return d;
}

uint16_t LeerDistanciaPulgada(void)
{
	//SE ENVIA LA SEÑAL ULTRASONIDO Y SE OBTIENE LA DIFERENCIA DE TIEMPO DE LA RECEPCION
	uint16_t t=0;
	uint16_t d=0;

	/* señal trigger*/
	//SETEO INICIALMENTE EN BAJO durante 5u
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,PORT(2),PIN(3));
	delayUs(TIMER(0),5);
	Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,PORT(2),PIN(3));
	delayUs(TIMER(0),10);
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,PORT(2),PIN(3));

	/*obtengo el tiempo de echo*/
	//tengo que esperar a que trigger reciba una señal en alto,
	//y empezar a contar su duracion hasta que vuelva a low

	//MIENTRAS ECHO ESTÉ EN BAJO NO HACER NADA
	do
	{

	}
	while (Chip_GPIO_ReadPortBit(LPC_GPIO_PORT,PORT(2),PIN(3))==0);

	while(Chip_GPIO_ReadPortBit(LPC_GPIO_PORT,PORT(2),PIN(3))==1)
	{
		t++;
		//retardo de 1u para obtener el tiempo en u
	}

	//solamente consideramos la mitad del tiempo en alto, que es la distancia hasta alcanzar el objeto sin
	//contar el tiempo de regreso
	t=t/2;

	/*distancia en centimetros*/
	d=t/148;

	return d;
}
void DesInicializarHcSr4(void)
{

}
