

#ifndef MODULES_LPC4337_M4_DRIVERS_APP_INC_TCRT5000_H_
#define MODULES_LPC4337_M4_DRIVERS_APP_INC_TCRT5000_H_

#include "chip.h"
#include "gpio_18xx_43xx.h"

void InicializarTcrt5000(void);
uint8_t LeerEstado(void);
void DesInicializarTcrt5000(void);


#endif /* MODULES_LPC4337_M4_DRIVERS_APP_INC_TCRT5000_H_ */

