/*
 * tcrt.h
 *
 *  Created on: 20 de ago. de 2018
 *      Author: facub
 */

#ifndef TCRT_H
#define TCRT_H

/*==================[inclusions]=============================================*/
#include "Tcrt5000.h"
#include "DisplayITS_E0803.h"
#include "retardos.h"

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

#endif //TCRT_H

