#ifndef RETARDOS_H
#define RETARDOS_H

#include "lpc_types.h"
#include "chip.h"

void delaySec(uint8_t timer_num, uint32_t delayInMs);
void delayMs(uint8_t timer_num, uint32_t delayInMs);
void delayUs(uint8_t timer_num, uint32_t delayInUs);

#endif /* RETARDOS_H */
