/*
 * Tcrt5000.c
 *
 *  Created on: 7 de ago. de 2018
 *      Author: facub
 */

#include "Tcrt5000.h"
#include "chip.h"

//#define GROUP(n) ((uint8_t) n)
//#define PORT(n) ((uint8_t) n)
//#define PIN(n) ((uint8_t) n)
#define TCRT_MUX_GROUP 	1
#define TCRT_MUX_PIN 	5
#define TCRT_GPIO_PORT	1
#define TCRT_GPIO_PIN	8
#define INPUT_DIR		0

	void InicializarTcrt5000(void)
	{
		/* Configuración del GPIO*/
		Chip_GPIO_Init(LPC_GPIO_PORT);
		Chip_SCU_PinMux(TCRT_MUX_GROUP,TCRT_MUX_PIN,MD_EZI|MD_ZI,FUNC0); // MD_EZI Y MD_ZI me permiten habilitar la entrada del trigger (habilitan el buffer) es NECESARIO para ENTRADA.
																		//selecciono funcion GPIO
		Chip_GPIO_SetDir(LPC_GPIO_PORT,TCRT_GPIO_PORT,1<<TCRT_GPIO_PIN,INPUT_DIR); // Determino GPIO como ENTRADA
	}

	uint8_t LeerEstado(void)
	{

		uint8_t estado=0;
		//lectura del pin para saber si hay linea oscura o no
		if (Chip_GPIO_ReadPortBit(LPC_GPIO_PORT, TCRT_GPIO_PORT, TCRT_GPIO_PIN))
		{
			//no se detecta linea oscura, la funcion devuelve 1
			estado=1;
		}
		else
			//hay linea oscura, la funcion devuelve 0
			estado=0;

		return estado;
	}
	void DesInicializarTcrt5000(void)
	{

	}
