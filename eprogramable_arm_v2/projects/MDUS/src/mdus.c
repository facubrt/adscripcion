/*
 * mdus.c
 *
 *  Created on: 23 de ago. de 2018
 *      Author: facub
 */

#include "mdus.h"
#include "board.h"
#include "chip.h"

#define TIMER(n)	((uint8_t) n)
int main ()
{
	SystemCoreClockUpdate();
	Board_Init();

	InicializarDisplay();
	InicializarHcSr4();

	uint16_t distancia=0;

	while(1)
	{
		distancia=LeerDistanciaCentimetro();
		MostrarValor(distancia);
		delaySec(TIMER(0),1);
	}
return 0;
}

