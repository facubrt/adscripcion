/*
 * hc_sr4.h
 *
 *  Created on: 23 de ago. de 2018
 *      Author: facub
 */

#include "chip.h"

#ifndef HC_SR4_H_
#define HC_SR4_H_


void InicializarHcSr4(void);
uint16_t LeerDistanciaCentimetro(void);
uint16_t LeerDistanciaPulgada(void);
void DesInicializarHcSr4(void);


#endif /* MODULES_LPC4337_M4_DRIVERS_APP_INC_HC_SR4_H_ */
