/* Copyright 2017, XXXXXXX
 * All rights reserved.
 *
 * This file is part of CIAA Firmware.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

#ifndef _DHT_H
#define _DTH_H

/*==================[inclusions]=============================================*/


#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

/*==================[external functions declaration]=========================*/

/** @} doxygen end group definition */
/** @} doxygen end group definition */
/** @} doxygen end group definition */
/*==================[end of file]============================================*/
/**
 * \file
 * \brief Interface to the DHT11/DHT22 temperature/humidity sensor.
 * \author Erich Styger
 */

#include "GPIO.h"
//#include "PE_Types.h"

#define DATOS GPIO7


/* either one of the defines below needs to be set to 1: */
#define DHTxx_SENSOR_TYPE_IS_DHT11  (1)
  /*!< Sensor is DHT11_SENSOR_DHT22 */
#define DHTxx_SENSOR_TYPE_IS_DHT22  (0)
  /*!< Sensor is DHT11_SENSOR_DHT22 */

#if DHTxx_SENSOR_TYPE_IS_DHT11
  #define DHTxx_SENSOR_PERIOD_MS    1000
    /*!< can only read sensor values with 1 Hz! */
#elif DHTxx_SENSOR_TYPE==DHTxx_SENSOR_DHT22
  #define DHTxx_SENSOR_PERIOD_MS    2000
    /*!< can only read sensor values with 0.5 Hz! */
#else
  #error "unknown device!"
#endif

/*! Error codes */
typedef enum {
  DHTxx_OK, /*!< OK */
  DHTxx_NO_PULLUP, /*!< no pull-up present */
  DHTxx_NO_ACK_0, /*!< no 0 acknowledge detected */
  DHTxx_NO_ACK_1, /*!< no 1 acknowledge detected */
  DHTxx_NO_DATA_0, /*!< low level expected during data transmission */
  DHTxx_NO_DATA_1, /*!< high level expected during data transmission */
  DHTxx_BAD_CRC,   /*!< bad CRC */
} DHTxx_ErrorCode;

void Init_DHT(void);
/*!
 * \brief Returns for a given error code the description string.
 * \param code Error Code
 * \return Error code description string
 */
const unsigned char *DHTxx_GetReturnCodeString(DHTxx_ErrorCode code);

/*!
 * \brief Reads the sensor data values
 * \param temperatureCentigrade Temperature value, in 1/100 units. E.g. 1517 is 15.17°C
 * \param humidityCentipercent Humidity value, in 1/100 units. E.g. 3756 is 37.56%
 * \return Error code
 */
DHTxx_ErrorCode DHTxx_Read(uint16_t *temperatureCentigrade, uint16_t *humidityCentipercent);

#endif /* DHTxx_H_ */

#endif /* #ifndef MI_NUEVO_PROYECTO_H */

