/*
 * mdus.h
 *
 *  Created on: 23 de ago. de 2018
 *      Author: facub
 */

#ifndef MDUS_H
#define MDUS_H

/*==================[inclusions]=============================================*/
#include "hc_sr4.h"
#include "DisplayITS_E0803.h"
#include "retardos.h"

#ifdef __cplusplus
extern "C" {
#endif

int main(void);

/*==================[cplusplus]==============================================*/

#ifdef __cplusplus
}
#endif

#endif //mdus.h

