/*
 * DisplayITS_E0803.c
 *
 *  Created on: 7 de ago. de 2018
 *      Author: facub
 */
/*==================[inclusions]=============================================*/

#include "../inc/DisplayITS_E0803.h"
#include "chip.h"

/*==================[macros and definitions]=================================*/

#define LCD_MUX_GROUP 	4
#define LCD1_MUX_PIN  	4
#define LCD2_MUX_PIN  	5
#define LCD3_MUX_PIN  	6
#define LCD4_MUX_PIN  	10
#define LCD_GPIO_PORT 	2
#define LCD4_GPIO_PORT	5
#define LCD1_GPIO_PIN 	4
#define LCD2_GPIO_PIN 	5
#define LCD3_GPIO_PIN 	6
#define LCD4_GPIO_PIN	14


#define GPIO_MUX_GROUP 	6
#define GPIO1_MUX_PIN  	4
#define GPIO3_MUX_PIN  	7
#define GPIO5_MUX_PIN  	9
#define GPIO1_GPIO_PORT	3
#define GPIO3_GPIO_PORT	5
#define GPIO5_GPIO_PORT	3
#define GPIO1_GPIO_PIN	3
#define GPIO3_GPIO_PIN	15
#define GPIO5_GPIO_PIN	5

#define INPUT_DIR 		0
#define OUTPUT_DIR 		1

#define MA				0x01
#define MB				0x02
#define MC				0x04
#define MD				0x08


/**/

/*==================[internal data definition]===============================

==================[internal functions declaration]=========================

==================[external data definition]===============================

==================[external functions definition]==========================*/

void InicializarDisplay(void)
{
	Chip_GPIO_Init(LPC_GPIO_PORT);

	//PINES LCD CONFIGURADO COMO GPIO, MODO PUP
	Chip_SCU_PinMux(4,4,MD_PUP,FUNC0);
	Chip_SCU_PinMux(4,5,MD_PUP,FUNC0);
	Chip_SCU_PinMux(4,6,MD_PUP,FUNC0);
	Chip_SCU_PinMux(4,10,MD_PUP,FUNC4);
	//PINES LCD CONFIGURADOS COMO GPIO SALIDA
	Chip_GPIO_SetDir(LPC_GPIO_PORT,2,1<<4,OUTPUT_DIR);
	Chip_GPIO_SetDir(LPC_GPIO_PORT,2,1<<5,OUTPUT_DIR);
	Chip_GPIO_SetDir(LPC_GPIO_PORT,2,1<<6,OUTPUT_DIR);
	Chip_GPIO_SetDir(LPC_GPIO_PORT,5,1<<14,OUTPUT_DIR);

	//PINES GPIO CONFIGURADOS COMO GPIO
	Chip_SCU_PinMux(GPIO_MUX_GROUP,GPIO1_MUX_PIN,MD_PUP,FUNC0);
	Chip_SCU_PinMux(GPIO_MUX_GROUP,GPIO3_MUX_PIN,MD_PUP,FUNC4);
	Chip_SCU_PinMux(GPIO_MUX_GROUP,GPIO5_MUX_PIN,MD_PUP,FUNC0);
	//PINES GPIO CONFIGURADOS COMO SALIDA
	Chip_GPIO_SetDir(LPC_GPIO_PORT,GPIO1_GPIO_PORT,1<<GPIO1_GPIO_PIN,OUTPUT_DIR);
	Chip_GPIO_SetDir(LPC_GPIO_PORT,GPIO3_GPIO_PORT,1<<GPIO3_GPIO_PIN,OUTPUT_DIR);
	Chip_GPIO_SetDir(LPC_GPIO_PORT,GPIO5_GPIO_PORT,1<<GPIO5_GPIO_PIN,OUTPUT_DIR);
}

void MostrarValor(uint16_t valor)
{
	//SETEO LAS LINEAS DE CONTROL EN BAJO
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,GPIO1_GPIO_PORT,GPIO1_GPIO_PIN);
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,GPIO3_GPIO_PORT,GPIO3_GPIO_PIN);
	Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,GPIO5_GPIO_PORT,GPIO5_GPIO_PIN);

	uint16_t centena=0;
	uint16_t decena=0;
	uint16_t unidad=0;

	//TRABAJO CON VALOR

	if(valor>100)
	{
		//CODIGO BCD 0000
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD1_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD2_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD3_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);

		//SELECCIONO CENTENA
		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,GPIO1_GPIO_PORT,GPIO1_GPIO_PIN);

		//ME QUEDO SOLAMENTE CON EL VALOR DE LA CENTENA
		centena=(valor/100);

		if((centena & MA)!=0) // EJ. 1001 & 0001
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD1_GPIO_PIN);

		if((centena & MB)!=0) // EJ. 1001 & 0010
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD2_GPIO_PIN);

		if((centena & MC)!=0) // EJ. 1001 & 0100
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD3_GPIO_PIN);

		if((centena & MD)!=0) // EJ. 1001 & 1000
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);

		//DESELECCIONO CENTENA
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,GPIO1_GPIO_PORT,GPIO1_GPIO_PIN);

		//CODIGO BCD 0000
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD1_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD2_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD3_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);

		//SELECCIONO DECENA
		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,GPIO3_GPIO_PORT,GPIO3_GPIO_PIN);

		//ME QUEDO CON LA DECENA DEL VALOR
		decena=((valor%100)/10);

		if((decena & MA)!=0) // EJ. 1001 & 0001
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD1_GPIO_PIN);

		if((decena & MB)!=0) // EJ. 1001 & 0010
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD2_GPIO_PIN);

		if((decena & MC)!=0) // EJ. 1001 & 0100
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD3_GPIO_PIN);

		if((decena & MD)!=0) // 1001 & 1000
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);

		//DESELECCIONO DECENA
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,GPIO3_GPIO_PORT,GPIO3_GPIO_PIN);

		//CODIGO BCD 0000
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD1_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD2_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD3_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);

		//SELECCIONO UNIDAD
		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,GPIO5_GPIO_PORT,GPIO5_GPIO_PIN);

		//ME QUEDO CON LA UNIDAD DEL VALOR
		unidad=((valor%100)%10);

		if((unidad & MA)!=0) // EJ. 1001 & 0001
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD1_GPIO_PIN);

		if((unidad & MB)!=0) // EJ. 1001 & 0010
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD2_GPIO_PIN);

		if((unidad & MC)!=0) // EJ. 1001 & 0100
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD3_GPIO_PIN);

		if((unidad & MD)!=0) // EJ. 1001 & 1000
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);

		//DESELECCIONO UNIDAD
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,GPIO5_GPIO_PORT,GPIO5_GPIO_PIN);

	}

	if((valor>10)&&(valor<100))
	{
		//CODIGO BCD 0000
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD1_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD2_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD3_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);

		//SELECCIONO DECENA
		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,GPIO3_GPIO_PORT,GPIO3_GPIO_PIN);
		//ME QUEDO CON LA DECENA DEL VALOR
		decena=(valor/10);

		if((decena & MA)!=0)
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD1_GPIO_PIN);

		if((decena & MB)!=0)
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD2_GPIO_PIN);

		if((decena & MC)!=0)
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD3_GPIO_PIN);

		if((decena & MD)!=0)
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);

		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,GPIO3_GPIO_PORT,GPIO3_GPIO_PIN);

		//CODIGO BCD 0000
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD1_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD2_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD3_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);

		//SELECCIONO UNIDAD
		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,GPIO5_GPIO_PORT,GPIO5_GPIO_PIN);

		//ME QUEDO CON LA UNIDAD DEL VALOR
		unidad=(valor%10);

		if((unidad & MA)!=0)
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD1_GPIO_PIN);

		if((unidad & MB)!=0)
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD2_GPIO_PIN);

		if((unidad & MC)!=0)
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD3_GPIO_PIN);

		if((unidad & MD)!=0)
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);

		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,GPIO5_GPIO_PORT,GPIO5_GPIO_PIN);
	}

	if (valor<10) //valor <10
	{
		//CODIGO BCD 0000
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD1_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD2_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD3_GPIO_PIN);
		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);

		Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,GPIO5_GPIO_PORT,GPIO5_GPIO_PIN);

		unidad=(valor);
		if((unidad & MA)!=0)
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD1_GPIO_PIN);

		if((unidad & MB)!=0)
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD2_GPIO_PIN);

		if((unidad & MC)!=0)
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD_GPIO_PORT,LCD3_GPIO_PIN);

		if((unidad & MD)!=0)
			Chip_GPIO_SetPinOutHigh(LPC_GPIO_PORT,LCD4_GPIO_PORT,LCD4_GPIO_PIN);

		Chip_GPIO_SetPinOutLow(LPC_GPIO_PORT,GPIO5_GPIO_PORT,GPIO5_GPIO_PIN);

	}
	//EN ESTA FUNCION DEBO TOMAR EL VALOR QUE ME PASAN, VER DIVIDIR POR 100 Y POR 10 PARA SEPARAR CENTENAS, DECENAS, UNIDADES
	// E IR ESCRIBIENDOLAS A MEDIDA QUE SELECCIONO LOS DIGITOS. A SU VEZ DEBO INICIAR CON LOS 3 SEL EN BAJO (DESHABILITADOS)

}
/*uint16_t LeerValorDisplayITS_E0803(void)
{
	//retorno simplemente el valor
}*/


/*==================[end of file]============================================*/


