/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "esp8266.h"       /* <= own header */

#include "board.h"



/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 30000


/*==================[internal functions declaration]=========================*/
void Delay(void);


void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}

int main(void)
{
	uint32_t tecla;
	uint8_t dato  = 0;

	Board_Init();
	Init_Leds();
	Init_Switches();
	Init_Uart_Ftdi();
	Init_Uart_Rs232();

	SendString_Uart_Ftdi("Interfaz de configuracion del modulo ESP8266\r\n");
	Delay();
	SendString_Uart_Ftdi("Tecla 1: Verifica comunicación con el modulo\r\n");
	Delay();
	SendString_Uart_Ftdi("Tecla 2: Informa version de firmware del modulo\r\n");
	Delay();
	SendString_Uart_Ftdi("Tecla 3: Lista los Access Points\r\n");
	Delay();
	SendString_Uart_Ftdi("Tecla 4: IP Address\r\n");
	Delay();
    while(1)
    {
    	tecla = Read_Switches();

    	switch(tecla)
    	{
    		case TEC1:
    			Led_Toggle(RGB_B_LED);
    			while(Read_Switches() == TEC1);
    			SendString_Uart_Rs232("AT\r\n");
    		break;
    		case TEC2:
    			Led_Toggle(RED_LED);
    			while(Read_Switches() == TEC2);
    			/*Informa la versión de Firmware del Módulo*/
    			SendString_Uart_Rs232("AT+GMR\r\n");
    		break;
    		case TEC3:
    			Led_Toggle(YELLOW_LED);
    			while(Read_Switches() == TEC3);
    			/*Lista los Acces Points*/
    			SendString_Uart_Rs232("AT+CWLAP\r\n");
    		break;
    		case TEC4:
    			Led_Toggle(GREEN_LED);
    			while(Read_Switches() == TEC4);
    			/*Dirección IP*/
    			SendString_Uart_Rs232("AT+CIFSR");
    		break;
    	}
    	if(ReadByte_Uart_Rs232(&dato))
    	{
    		SendByte_Uart_Ftdi(&dato);
    	}
	}
    
	return 0;
}

/*==================[end of file]============================================*/

