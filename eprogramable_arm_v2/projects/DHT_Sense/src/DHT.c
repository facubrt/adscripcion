/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 * Gonzalo Cuenca  - cuencagonzalo@gmail.com
 * extraido de:
 * https://mcuoneclipse.com/2015/03/27/using-the-dht11-temperaturehumidity-sensor-with-a-frdm-board/
 * Revisión:
 * 17-02-18: Versión inicial
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/DHT.h"       /* <= own header */

#include "board.h"

/*==================[macros and definitions]=================================*/
#define COUNT_DELAY 3000000

/*==================[internal data definition]===============================*/



/*==================[internal functions declaration]=========================*/

void Delay(void)
{
	uint32_t i;

	for(i=COUNT_DELAY; i!=0; i--)
	{
		   asm  ("nop");
	}
}

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/

/**
 * \file
 * \brief Implementation of a driver for the DHT11 temperature/humidity sensor.
 * \author Erich Styger
 */
//#include "WAIT1.h"
//#include "Data.h"
void Init_DHT()
{
	Init_GPIO();

}

const unsigned char *DHTxx_GetReturnCodeString(DHTxx_ErrorCode code) {
  switch(code) {
    case DHTxx_OK:        return "OK";
    case DHTxx_NO_PULLUP: return "NO_PULLUP";
    case DHTxx_NO_ACK_0:  return "NO_ACK_0";
    case DHTxx_NO_ACK_1:  return "NO_ACK_1";
    case DHTxx_NO_DATA_0: return "NO_DATA_0";
    case DHTxx_NO_DATA_1: return "NO_DATA_1";
    case DHTxx_BAD_CRC:   return "BAD_CRC";
    default:              return "UNKNOWN?";
  }
}

DHTxx_ErrorCode DHTxx_Read(uint16_t *temperatureCentigrade, uint16_t *humidityCentipercent) {
  int cntr;
  int loopBits;
  uint8_t buffer[5];
  int i;
  int data;

  /* init buffer */
  for(i=0;i<sizeof(buffer); i++) {
    buffer[i] = 0;
  }
  //EnterCritical(); /* disabling interrupts */
  /* Disabling interrupts so we do not get interrupted. Note that this is for a about 25 ms!
   * Alternatively only disable interrupts during sampling the data bits, and not during the first 18 ms.
   */
  /* set to input and check if the signal gets pulled up */
  GPIO_Set_Dir(DATOS,INPUT);
  WAIT1_Waitus(50);
  if(GPIO_Read(DATOS)==0) {
    return DHTxx_NO_PULLUP;
  }
  /* send start signal */
  GPIO_Set_Dir(DATOS,OUTPUT);
  GPIO_Write(DATOS,OFF);
  WAIT1_Waitms(18); /* keep signal low for at least 18 ms */
  GPIO_Set_Dir(DATOS,INPUT);
  WAIT1_Waitus(50);
  /* check for acknowledge signal */
  if (GPIO_Read(DATOS)!=0) { /* signal must be pulled low by the sensor */
    return DHTxx_NO_ACK_0;
  }
  /* wait max 100 us for the ack signal from the sensor */
  cntr = 18;
  while(GPIO_Read(DATOS)==0) { /* wait until signal goes up */
    WAIT1_Waitus(5);
    if (--cntr==0) {
      return DHTxx_NO_ACK_1; /* signal should be up for the ACK here */
    }
  }
  /* wait until it goes down again, end of ack sequence */
  cntr = 18;
  while(GPIO_Read(DATOS)!=0) { /* wait until signal goes down */
    WAIT1_Waitus(5);
    if (--cntr==0) {
      return DHTxx_NO_ACK_0; /* signal should be down to zero again here */
    }
  }
  /* now read the 40 bit data */
  i = 0;
  data = 0;
  loopBits = 40;
  do {
    cntr = 11; /* wait max 55 us */
    while(GPIO_Read(DATOS)==0) {
      WAIT1_Waitus(5);
      if (--cntr==0) {
        return DHTxx_NO_DATA_0;
      }
    }
    cntr = 15; /* wait max 75 us */
    while(GPIO_Read(DATOS)!=0) {
      WAIT1_Waitus(5);
      if (--cntr==0) {
        return DHTxx_NO_DATA_1;
      }
    }
    data <<= 1; /* next data bit */
    if (cntr<10) { /* data signal high > 30 us ==> data bit 1 */
      data |= 1;
    }
    if ((loopBits&0x7)==1) { /* next byte */
      buffer[i] = data;
      i++;
      data = 0;
    }
  } while(--loopBits!=0);
 // ExitCritical(); /* re-enabling interrupts */

  /* now we have the 40 bit (5 bytes) data:
   * byte 1: humidity integer data
   * byte 2: humidity decimal data (not used for DTH11, always zero)
   * byte 3: temperature integer data
   * byte 4: temperature fractional data (not used for DTH11, always zero)
   * byte 5: checksum, the sum of byte 1 + 2 + 3 + 4
   */
  /* test CRC */
  if (buffer[0]+buffer[1]+buffer[2]+buffer[3]!=buffer[4]) {
    return DHTxx_BAD_CRC;
  }
  /* store data values for caller */
#if DHTxx_SENSOR_TYPE_IS_DHT11
  *humidityCentipercent = ((int)buffer[0])*100;
  *temperatureCentigrade = ((int)buffer[2])*100;
#else
  *humidityCentipercent = (((int)buffer[0]<<8)+buffer[1])*10;
  *temperatureCentigrade = (((int)buffer[2]<<8)+buffer[3])*10;
#endif
  return DHTxx_OK;
}
/*==================[end of file]============================================*/

