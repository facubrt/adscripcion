/*
 * tcrt.c
 *
 *  Created on: 20 de ago. de 2018
 *      Author: facub
 */

#include "tcrt.h"
#include "board.h"
#include "chip.h"

#define TIMER(n) ((uint8_t) n)

int main ()
{
	uint8_t estado=0;
	uint8_t cont=0;

	SystemCoreClockUpdate();
	Board_Init();

	InicializarDisplay();
	InicializarTcrt5000();

	while(1)
	{
		estado=LeerEstado();
		if (!estado) //se detecta linea oscura, por lo que se incrementa el contador
		{
			cont++;
		}
		MostrarValor(cont);
		delaySec(TIMER(0),2);
	}

	return 0;
}
